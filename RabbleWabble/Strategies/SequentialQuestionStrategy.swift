//
//  SequentialQuestionStrategy.swift
//  RabbleWabble
//
//  Created by 陳信毅 on 2018/9/26.
//  Copyright © 2018年 陳信毅. All rights reserved.
//

import Foundation

public class SequentialQuestionStrategy: BaseQuestionStrategy {
    
    public convenience init(
        questionGroupCaretaker: QuestionGroupCaretaker) {
        let questionGroup =
            questionGroupCaretaker.selectedQuestionGroup!
        let questions = questionGroup.questions
        self.init(questionGroupCaretaker: questionGroupCaretaker,
                  questions: questions)
    }
}
