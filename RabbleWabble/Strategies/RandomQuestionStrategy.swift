//
//  RandomQuestionStrategy.swift
//  RabbleWabble
//
//  Created by 陳信毅 on 2018/9/26.
//  Copyright © 2018年 陳信毅. All rights reserved.
//

import Foundation
// 1
import GameplayKit.GKRandomSource

public class RandomQuestionStrategy: BaseQuestionStrategy {
    
    public convenience init(
        questionGroupCaretaker: QuestionGroupCaretaker) {
        let questionGroup =
            questionGroupCaretaker.selectedQuestionGroup!
        let randomSource = GKRandomSource.sharedRandom()
        let questions = randomSource.arrayByShufflingObjects(
            in: questionGroup.questions) as! [Question]
        self.init(questionGroupCaretaker: questionGroupCaretaker,
                  questions: questions)
    }
}
