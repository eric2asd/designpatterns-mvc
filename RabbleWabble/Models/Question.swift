//
//  Question.swift
//  RabbleWabble
//
//  Created by 陳信毅 on 2018/9/14.
//  Copyright © 2018年 陳信毅. All rights reserved.
//

import Foundation

public class Question: Codable {
    public let answer: String
    public let hint: String?
    public let prompt: String
    
    public init(answer: String, hint: String?, prompt: String) {
        self.answer = answer
        self.hint = hint
        self.prompt = prompt
    }
}
