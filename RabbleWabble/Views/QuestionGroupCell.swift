//
//  QuestionGroupCell.swift
//  RabbleWabble
//
//  Created by 陳信毅 on 2018/9/26.
//  Copyright © 2018年 陳信毅. All rights reserved.
//

import UIKit

public class QuestionGroupCell: UITableViewCell {
    @IBOutlet public var titleLabel: UILabel!
    @IBOutlet public var percentageLabel: UILabel!
}
